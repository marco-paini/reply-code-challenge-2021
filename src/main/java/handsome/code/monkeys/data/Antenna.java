package handsome.code.monkeys.data;

import java.awt.Point;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@PlanningEntity
public class Antenna {
	/** The connection speed of the i th antenna. */
	@Max(10000)
	@Min(1)
	@Setter(AccessLevel.NONE)
	private Integer C;

	/** The id of the i th antenna to be placed. */
	@PlanningId
	@Setter(AccessLevel.NONE)
	private Integer id;

	/** The range of the i th antenna. */
	@Max(6000)
	@Min(0)
	@Setter(AccessLevel.NONE)
	private Integer R;

	/** The x coordinate of the i th antenna. */
	@Min(0)
	private Integer X;

	/** The y coordinate of the i th antenna. */
	@Min(0)
	private Integer Y;

	public Antenna(final Integer C, final Integer id, final Integer R) {
		this.C = C;
		this.id = id;
		this.R = R;
	}

	@PlanningVariable(nullable = true, valueRangeProviderRefs = { "points" })
	public Point getPoint() {
		return Stream.of(X, Y).anyMatch(Objects::isNull) ? null : new Point(X, Y);
	}

	public void setPoint(final Point point) {
		Optional.ofNullable(point).ifPresentOrElse(p -> {
			X = p.x;
			Y = p.y;
		}, () -> X = Y = null);
	}
}
