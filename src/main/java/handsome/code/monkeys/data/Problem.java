package handsome.code.monkeys.data;

import java.awt.Point;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@PlanningSolution
public class Problem {
	@PlanningEntityCollectionProperty
	private List<Antenna> A = new ArrayList<>();

	@ProblemFactCollectionProperty
	private List<Building> B = new ArrayList<>();

	/** The height of the grid. */
	@Max(6000)
	@Min(10)
	private Integer H;

	/** The number of available antennas that can be placed in the grid. */
	@Max(60000)
	@Min(1)
	private Integer M;

	/** The number of buildings present in the grid. */
	@Max(350000)
	@Min(1)
	private Integer N;

	@ProblemFactCollectionProperty
	@ToString.Exclude
	@ValueRangeProvider(id = "points")
	private List<Point> points;

	/** The reward assigned if all the buildings are connected to the network. */
	@Max(200000000)
	@Min(1)
	@ProblemFactProperty
	private Integer R;

	@PlanningScore
	private HardSoftLongScore score;

	/** The width of the grid. */
	@Max(6000)
	@Min(10)
	private Integer W;

	@SneakyThrows
	public Problem(final Path path) {
		Files.lines(path).forEach(line -> {
			final List<Integer> numbers = Stream.of(line.split(" ")).map(Integer::valueOf).collect(Collectors.toList());
			if (Stream.of(H, W).anyMatch(Objects::isNull)) {
				H = numbers.get(1);
				W = numbers.get(0);
			} else if (Stream.of(M, N, R).anyMatch(Objects::isNull)) {
				M = numbers.get(1);
				N = numbers.get(0);
				R = numbers.get(2);
			} else if (B.size() < N) {
				B.add(new Building(numbers.get(3), numbers.get(2), numbers.get(0), numbers.get(1)));
			} else if (A.size() < M) {
				A.add(new Antenna(numbers.get(1), A.size(), numbers.get(0)));
			}
		});
		IntStream.range(0, Math.min(getPoints().size(), A.size())).forEach(i -> A.get(i).setPoint(getPoints().get(i)));
	}

	public List<Point> getPoints() {
		return Optional.ofNullable(points)
				.orElseGet(() -> points = B.stream().map(Building::getPoint).collect(Collectors.toList()));
	}
}
