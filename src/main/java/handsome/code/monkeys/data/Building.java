package handsome.code.monkeys.data;

import java.awt.Point;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Value
public class Building {
	/** The connection speed weight of the i th building. */
	@Max(100)
	@Min(0)
	Integer C;

	/** The latency weight of the i th building. */
	@Max(100)
	@Min(0)
	Integer L;

	@Getter(lazy = true)
	@ToString.Exclude
	Point point = point();

	/** The x coordinate of the i th building. */
	@Min(0)
	Integer X;

	/** The y coordinate of the i th building. */
	@Min(0)
	Integer Y;

	private Point point() {
		return new Point(X, Y);
	}
}
