package handsome.code.monkeys;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;

import handsome.code.monkeys.data.Antenna;
import handsome.code.monkeys.data.Problem;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import lombok.SneakyThrows;

@QuarkusMain
public class Main implements QuarkusApplication {
	@ConfigProperty(defaultValue = "6")
	long limit;

	@Inject
	Logger log;

	private final String output = System.getProperty("user.dir") + "/src/main/resources/output/";

	@ConfigProperty(defaultValue = "0")
	long skip;

	@Inject
	SolverManager<Problem, Path> solverManager;

	private String input(final String string) {
		return "/input/" + string;
	}

	@Override
	public int run(String... args) throws Exception {
		Stream.of("data_scenarios_a_example.in", "data_scenarios_b_mumbai.in", "data_scenarios_c_metropolis.in",
				"data_scenarios_d_polynesia.in", "data_scenarios_e_sanfrancisco.in", "data_scenarios_f_tokyo.in")
				.skip(skip).limit(limit).map(this::input).map(getClass().getClassLoader()::getResource)
				.map(URL::getPath).map(Paths::get).map(this::solve).forEach(this::write);
		return 0;
	}

	@SneakyThrows
	private SolverJob<Problem, Path> solve(final Path path) {
		return solverManager.solve(path, new Problem(path));
	}

	@SneakyThrows
	private void write(final SolverJob<Problem, Path> solverJob) {
		final Path path = solverJob.getProblemId().getFileName();
		final Problem problem = solverJob.getFinalBestSolution();
		final List<Antenna> antennas = problem.getA().stream()
				.filter(antenna -> Stream.of(antenna.getX(), antenna.getY()).allMatch(Objects::nonNull))
				.collect(Collectors.toList());
		log.info("=== " + path + " ===");
		log.info(problem.toString());
		Files.write(Paths.get(output + path), Stream
				.concat(Stream.of(antennas).map(List::size).map(String::valueOf),
						antennas.stream()
								.map(antenna -> Stream.of(antenna.getId(), antenna.getX(), antenna.getY())
										.map(String::valueOf).collect(Collectors.joining(" "))))
				.collect(Collectors.toList()));
	}
}
